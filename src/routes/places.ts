export interface IPlace {
	name: string;
	desc?: string;
}

export const places: IPlace[] = [
	{ name: 'Place1', desc: 'Desc for place 1' },
	{ name: 'Place2', desc: 'Desc for place 2' }
];
